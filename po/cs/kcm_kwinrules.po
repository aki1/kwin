# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2010, 2011, 2012, 2013, 2014, 2015, 2017.
# Lukáš Tinkl <ltinkl@redhat.com>, 2010, 2011.
# Tomáš Chvátal <tomas.chvatal@gmail.com>, 2012, 2013.
# Vit Pelcak <vit@pelcak.org>, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmkwinrules\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-07 02:19+0000\n"
"PO-Revision-Date: 2023-06-05 13:11+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.1\n"
"X-Language: cs_CZ\n"
"X-Source-Language: en_US\n"

#: kcmrules.cpp:226
#, kde-format
msgid "Copy of %1"
msgstr "Kopie %1"

#: kcmrules.cpp:406
#, kde-format
msgid "Application settings for %1"
msgstr "Nastavení aplikace pro %1"

#: kcmrules.cpp:428 rulesmodel.cpp:215
#, kde-format
msgid "Window settings for %1"
msgstr "Nastavení okna pro %1"

#: optionsmodel.cpp:198
#, kde-format
msgid "Unimportant"
msgstr "Nedůležité"

#: optionsmodel.cpp:199
#, kde-format
msgid "Exact Match"
msgstr "Přesná shoda"

#: optionsmodel.cpp:200
#, kde-format
msgid "Substring Match"
msgstr "Shoda podřetězce"

#: optionsmodel.cpp:201
#, kde-format
msgid "Regular Expression"
msgstr "Regulární výraz"

#: optionsmodel.cpp:205
#, kde-format
msgid "Apply Initially"
msgstr "Použít na začátku"

#: optionsmodel.cpp:206
#, kde-format
msgid ""
"The window property will be only set to the given value after the window is "
"created.\n"
"No further changes will be affected."
msgstr ""

#: optionsmodel.cpp:209
#, kde-format
msgid "Apply Now"
msgstr "Aplikovat nyní"

#: optionsmodel.cpp:210
#, kde-format
msgid ""
"The window property will be set to the given value immediately and will not "
"be affected later\n"
"(this action will be deleted afterwards)."
msgstr ""

#: optionsmodel.cpp:213
#, kde-format
msgid "Remember"
msgstr "Zapamatovat si"

#: optionsmodel.cpp:214
#, kde-format
msgid ""
"The value of the window property will be remembered and, every time the "
"window is created, the last remembered value will be applied."
msgstr ""

#: optionsmodel.cpp:217
#, kde-format
msgid "Do Not Affect"
msgstr "Nemá vliv"

#: optionsmodel.cpp:218
#, kde-format
msgid ""
"The window property will not be affected and therefore the default handling "
"for it will be used.\n"
"Specifying this will block more generic window settings from taking effect."
msgstr ""

#: optionsmodel.cpp:221
#, kde-format
msgid "Force"
msgstr "Vynutit"

#: optionsmodel.cpp:222
#, kde-format
msgid "The window property will be always forced to the given value."
msgstr "Vlastnost okna bude vždy vynucena na danou hodnotu."

#: optionsmodel.cpp:224
#, kde-format
msgid "Force Temporarily"
msgstr "Dočasně vynutit"

#: optionsmodel.cpp:225
#, kde-format
msgid ""
"The window property will be forced to the given value until it is hidden\n"
"(this action will be deleted after the window is hidden)."
msgstr ""

#: rulesmodel.cpp:218
#, kde-format
msgid "Settings for %1"
msgstr "Nastavení pro %1"

#: rulesmodel.cpp:221
#, kde-format
msgid "New window settings"
msgstr "Nové nastavení okna"

#: rulesmodel.cpp:237
#, kde-format
msgid ""
"You have specified the window class as unimportant.\n"
"This means the settings will possibly apply to windows from all "
"applications. If you really want to create a generic setting, it is "
"recommended you at least limit the window types to avoid special window "
"types."
msgstr ""
"Nastavili jste třídu okna jako nedůležitou.\n"
"Tato nastavení se pravděpodobně použijí na všechna okna aplikací. Pokud si "
"skutečně přejete vytvořit generické nastavení, je doporučeno omezit alespoň "
"typ oken a zabránit tak zvláštním typům oken."

#: rulesmodel.cpp:244
#, kde-format
msgid ""
"Some applications set their own geometry after starting, overriding your "
"initial settings for size and position. To enforce these settings, also "
"force the property \"%1\" to \"Yes\"."
msgstr ""
"Některé aplikace nastavují vlastní rozměry po spuštění, čímž přepíší vaše "
"původní nastavení pro velikost a umístění. Abyste tato nastavení vynutili, "
"vynuťte taky vlastnost \"%1\" na \"Ano\"."

#: rulesmodel.cpp:251
#, kde-format
msgid ""
"Readability may be impaired with extremely low opacity values. At 0%, the "
"window becomes invisible."
msgstr ""

#: rulesmodel.cpp:382
#, kde-format
msgid "Description"
msgstr "Popis"

#: rulesmodel.cpp:382 rulesmodel.cpp:390 rulesmodel.cpp:398 rulesmodel.cpp:405
#: rulesmodel.cpp:411 rulesmodel.cpp:419 rulesmodel.cpp:424 rulesmodel.cpp:430
#, kde-format
msgid "Window matching"
msgstr "Shoda okna"

#: rulesmodel.cpp:390
#, kde-format
msgid "Window class (application)"
msgstr "Třída okna (aplikace)"

#: rulesmodel.cpp:398
#, kde-format
msgid "Match whole window class"
msgstr "Odpovídá celé třídě okna"

#: rulesmodel.cpp:405
#, kde-format
msgid "Whole window class"
msgstr "Celá třída okna"

#: rulesmodel.cpp:411
#, kde-format
msgid "Window types"
msgstr "Typy oken"

#: rulesmodel.cpp:419
#, kde-format
msgid "Window role"
msgstr "Role okna"

#: rulesmodel.cpp:424
#, kde-format
msgid "Window title"
msgstr "Titulek okna"

#: rulesmodel.cpp:430
#, kde-format
msgid "Machine (hostname)"
msgstr "Počítač (název)"

#: rulesmodel.cpp:436
#, kde-format
msgid "Position"
msgstr "Pozice"

#: rulesmodel.cpp:436 rulesmodel.cpp:442 rulesmodel.cpp:448 rulesmodel.cpp:453
#: rulesmodel.cpp:461 rulesmodel.cpp:467 rulesmodel.cpp:486 rulesmodel.cpp:502
#: rulesmodel.cpp:507 rulesmodel.cpp:512 rulesmodel.cpp:517 rulesmodel.cpp:522
#: rulesmodel.cpp:531 rulesmodel.cpp:546 rulesmodel.cpp:551 rulesmodel.cpp:556
#, kde-format
msgid "Size & Position"
msgstr "Velikost a umístění"

#: rulesmodel.cpp:442
#, kde-format
msgid "Size"
msgstr "Velikost"

#: rulesmodel.cpp:448
#, kde-format
msgid "Maximized horizontally"
msgstr "Maximalizované vodorovně"

#: rulesmodel.cpp:453
#, kde-format
msgid "Maximized vertically"
msgstr "Maximalizované svisle"

#: rulesmodel.cpp:461
#, kde-format
msgid "Virtual Desktop"
msgstr "Virtuální plocha"

#: rulesmodel.cpp:467
#, kde-format
msgid "Virtual Desktops"
msgstr "Virtuální plochy"

#: rulesmodel.cpp:486
#, kde-format
msgid "Activities"
msgstr "Aktivity"

#: rulesmodel.cpp:502
#, kde-format
msgid "Screen"
msgstr "Obrazovka"

#: rulesmodel.cpp:507
#, kde-format
msgid "Fullscreen"
msgstr "Celá obrazovka"

#: rulesmodel.cpp:512
#, kde-format
msgid "Minimized"
msgstr "Minimalizované"

#: rulesmodel.cpp:517
#, kde-format
msgid "Shaded"
msgstr "Sbalené"

#: rulesmodel.cpp:522
#, kde-format
msgid "Initial placement"
msgstr "Úvodní umístění"

#: rulesmodel.cpp:531
#, kde-format
msgid "Ignore requested geometry"
msgstr "Ignorovat požadovanou geometrii"

#: rulesmodel.cpp:534
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some applications can set their own geometry, overriding the window manager "
"preferences. Setting this property overrides their placement requests.<nl/"
"><nl/>This affects <interface>Size</interface> and <interface>Position</"
"interface> but not <interface>Maximized</interface> or "
"<interface>Fullscreen</interface> states.<nl/><nl/>Note that the position "
"can also be used to map to a different <interface>Screen</interface>"
msgstr ""

#: rulesmodel.cpp:546
#, kde-format
msgid "Minimum Size"
msgstr "Minimální velikost"

#: rulesmodel.cpp:551
#, kde-format
msgid "Maximum Size"
msgstr "Maximální velikost"

#: rulesmodel.cpp:556
#, kde-format
msgid "Obey geometry restrictions"
msgstr "Řídit se omezenou geometrií"

#: rulesmodel.cpp:558
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some apps like video players or terminals can ask KWin to constrain them to "
"certain aspect ratios or only grow by values larger than the dimensions of "
"one character. Use this property to ignore such restrictions and allow those "
"windows to be resized to arbitrary sizes.<nl/><nl/>This can be helpful for "
"windows that can't quite fit the full screen area when maximized."
msgstr ""

#: rulesmodel.cpp:569
#, kde-format
msgid "Keep above other windows"
msgstr "Ponechat nad jinými okny"

#: rulesmodel.cpp:569 rulesmodel.cpp:574 rulesmodel.cpp:579 rulesmodel.cpp:585
#: rulesmodel.cpp:591 rulesmodel.cpp:597
#, kde-format
msgid "Arrangement & Access"
msgstr "Uspořádání a přístup"

#: rulesmodel.cpp:574
#, kde-format
msgid "Keep below other windows"
msgstr "Podržet pod ostatními okny"

#: rulesmodel.cpp:579
#, kde-format
msgid "Skip taskbar"
msgstr "Přeskočit panel úloh"

#: rulesmodel.cpp:581
#, kde-format
msgctxt "@info:tooltip"
msgid "Controls whether or not the window appears in the Task Manager."
msgstr ""

#: rulesmodel.cpp:585
#, kde-format
msgid "Skip pager"
msgstr "Přeskočit přepínač ploch"

#: rulesmodel.cpp:587
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the Virtual Desktop manager."
msgstr ""

#: rulesmodel.cpp:591
#, kde-format
msgid "Skip switcher"
msgstr "Přeskočit přepínač"

#: rulesmodel.cpp:593
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the <shortcut>Alt+Tab</"
"shortcut> window list."
msgstr ""

#: rulesmodel.cpp:597
#, kde-format
msgid "Shortcut"
msgstr "Zkratka"

#: rulesmodel.cpp:603
#, kde-format
msgid "No titlebar and frame"
msgstr "Bez titulku a rámu"

#: rulesmodel.cpp:603 rulesmodel.cpp:608 rulesmodel.cpp:614 rulesmodel.cpp:619
#: rulesmodel.cpp:625 rulesmodel.cpp:652 rulesmodel.cpp:680 rulesmodel.cpp:686
#: rulesmodel.cpp:698 rulesmodel.cpp:703 rulesmodel.cpp:709 rulesmodel.cpp:714
#, kde-format
msgid "Appearance & Fixes"
msgstr "Vzhled a opravy"

#: rulesmodel.cpp:608
#, kde-format
msgid "Titlebar color scheme"
msgstr "Barevné schéma titulkového pruhu"

#: rulesmodel.cpp:614
#, kde-format
msgid "Active opacity"
msgstr "Aktivní krytí"

#: rulesmodel.cpp:619
#, kde-format
msgid "Inactive opacity"
msgstr "Neaktivní krytí"

#: rulesmodel.cpp:625
#, kde-format
msgid "Focus stealing prevention"
msgstr "Prevence ztráty zaměření"

#: rulesmodel.cpp:627
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"KWin tries to prevent windows that were opened without direct user action "
"from raising themselves and taking focus while you're currently interacting "
"with another window. This property can be used to change the level of focus "
"stealing prevention applied to individual windows and apps.<nl/><nl/>Here's "
"what will happen to a window opened without your direct action at each level "
"of focus stealing prevention:<nl/><list><item><emphasis strong='true'>None:</"
"emphasis> The window will be raised and focused.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied, but "
"in the case of a situation KWin considers ambiguous, the window will be "
"raised and focused.</item><item><emphasis strong='true'>Normal:</emphasis> "
"Focus stealing prevention will be applied, but  in the case of a situation "
"KWin considers ambiguous, the window will <emphasis>not</emphasis> be raised "
"and focused.</item><item><emphasis strong='true'>High:</emphasis> The window "
"will only be raised and focused if it belongs to the same app as the "
"currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> The window will never be raised and focused.</item></list>"
msgstr ""

#: rulesmodel.cpp:652
#, kde-format
msgid "Focus protection"
msgstr "Ochrana zaměření"

#: rulesmodel.cpp:654
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"This property controls the focus protection level of the currently active "
"window. It is used to override the focus stealing prevention applied to new "
"windows that are opened without your direct action.<nl/><nl/>Here's what "
"happens to new windows that are opened without your direct action at each "
"level of focus protection while the window with this property applied to it "
"has focus:<nl/><list><item><emphasis strong='true'>None</emphasis>: Newly-"
"opened windows always raise themselves and take focus.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied to "
"the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will be raised and focused.</item><item><emphasis "
"strong='true'>Normal:</emphasis> Focus stealing prevention will be applied "
"to the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will <emphasis>not</emphasis> be raised and focused.</"
"item><item><emphasis strong='true'>High:</emphasis> Newly-opened windows "
"will only raise themselves and take focus if they belongs to the same app as "
"the currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> Newly-opened windows never raise themselves and take focus.</"
"item></list>"
msgstr ""

#: rulesmodel.cpp:680
#, kde-format
msgid "Accept focus"
msgstr "Přijmout zaměření"

#: rulesmodel.cpp:682
#, kde-format
msgid "Controls whether or not the window becomes focused when clicked."
msgstr ""

#: rulesmodel.cpp:686
#, kde-format
msgid "Ignore global shortcuts"
msgstr "Ignorovat globální zkratky"

#: rulesmodel.cpp:688
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Use this property to prevent global keyboard shortcuts from working while "
"the window is focused. This can be useful for apps like emulators or virtual "
"machines that handle some of the same shortcuts themselves.<nl/><nl/>Note "
"that you won't be able to <shortcut>Alt+Tab</shortcut> out of the window or "
"use any other global shortcuts such as <shortcut>Alt+Space</shortcut> to "
"activate KRunner."
msgstr ""

#: rulesmodel.cpp:698
#, kde-format
msgid "Closeable"
msgstr "Lze zavřít"

#: rulesmodel.cpp:703
#, kde-format
msgid "Set window type"
msgstr "Nastavit typ okna na"

#: rulesmodel.cpp:709
#, kde-format
msgid "Desktop file name"
msgstr "Název souboru plochy"

#: rulesmodel.cpp:714
#, kde-format
msgid "Block compositing"
msgstr "Zablokovat kompozici"

#: rulesmodel.cpp:766
#, kde-format
msgid "Window class not available"
msgstr "Třída okna není dostupná"

#: rulesmodel.cpp:767
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application is not providing a class for the window, so KWin cannot use "
"it to match and apply any rules. If you still want to apply some rules to "
"it, try to match other properties like the window title instead.<nl/><nl/"
">Please consider reporting this bug to the application's developers."
msgstr ""

#: rulesmodel.cpp:801
#, kde-format
msgid "All Window Types"
msgstr "Všechny typy oken"

#: rulesmodel.cpp:802
#, kde-format
msgid "Normal Window"
msgstr "Normální okno"

#: rulesmodel.cpp:803
#, kde-format
msgid "Dialog Window"
msgstr "Dialogové okno"

#: rulesmodel.cpp:804
#, kde-format
msgid "Utility Window"
msgstr "Nástrojové okno"

#: rulesmodel.cpp:805
#, kde-format
msgid "Dock (panel)"
msgstr "Dok (panel)"

#: rulesmodel.cpp:806
#, kde-format
msgid "Toolbar"
msgstr "Panel nástrojů"

#: rulesmodel.cpp:807
#, kde-format
msgid "Torn-Off Menu"
msgstr "Vytržená nabídka"

#: rulesmodel.cpp:808
#, kde-format
msgid "Splash Screen"
msgstr "Úvodní obrazovka"

#: rulesmodel.cpp:809
#, kde-format
msgid "Desktop"
msgstr "Pracovní plocha"

#. i18n("Unmanaged Window")},  deprecated
#: rulesmodel.cpp:811
#, kde-format
msgid "Standalone Menubar"
msgstr "Samostatná nabídka"

#: rulesmodel.cpp:812
#, kde-format
msgid "On Screen Display"
msgstr "On Screen Display"

#: rulesmodel.cpp:822
#, kde-format
msgid "All Desktops"
msgstr "Všechny plochy"

#: rulesmodel.cpp:824
#, kde-format
msgctxt "@info:tooltip in the virtual desktop list"
msgid "Make the window available on all desktops"
msgstr "Zpřístupnit okna na všech plochách"

#: rulesmodel.cpp:843
#, kde-format
msgid "All Activities"
msgstr "Všechny aktivity"

#: rulesmodel.cpp:845
#, kde-format
msgctxt "@info:tooltip in the activity list"
msgid "Make the window available on all activities"
msgstr "Zpřístupnit okno ve všech aktivitách"

#: rulesmodel.cpp:866
#, kde-format
msgid "Default"
msgstr "Výchozí"

#: rulesmodel.cpp:867
#, kde-format
msgid "No Placement"
msgstr "Bez umístění"

#: rulesmodel.cpp:868
#, kde-format
msgid "Minimal Overlapping"
msgstr "Minimální překrytí"

#: rulesmodel.cpp:869
#, kde-format
msgid "Maximized"
msgstr "Maximalizováno"

#: rulesmodel.cpp:870
#, kde-format
msgid "Centered"
msgstr "Na střed"

#: rulesmodel.cpp:871
#, kde-format
msgid "Random"
msgstr "Náhodné"

#: rulesmodel.cpp:872
#, kde-format
msgid "In Top-Left Corner"
msgstr "V levém horním rohu"

#: rulesmodel.cpp:873
#, kde-format
msgid "Under Mouse"
msgstr "Pod myší"

#: rulesmodel.cpp:874
#, kde-format
msgid "On Main Window"
msgstr "Na hlavním okně"

# žádné parametry funkce v inspektoru funkcí
#: rulesmodel.cpp:881
#, kde-format
msgid "None"
msgstr "Nic"

#: rulesmodel.cpp:882
#, kde-format
msgid "Low"
msgstr "Nízká"

#: rulesmodel.cpp:883
#, kde-format
msgid "Normal"
msgstr "Normální"

#: rulesmodel.cpp:884
#, kde-format
msgid "High"
msgstr "Vysoká"

#: rulesmodel.cpp:885
#, kde-format
msgid "Extreme"
msgstr "Extrémní"

#: rulesmodel.cpp:928
#, kde-format
msgid "Unmanaged window"
msgstr "Nespravované okno"

#: rulesmodel.cpp:929
#, kde-format
msgid "Could not detect window properties. The window is not managed by KWin."
msgstr ""

#: ui/FileDialogLoader.qml:15
#, kde-format
msgid "Select File"
msgstr "Vybrat soubor"

#: ui/FileDialogLoader.qml:27
#, kde-format
msgid "KWin Rules (*.kwinrule)"
msgstr "Pravidla KWinu (*.kwinrule)"

#: ui/main.qml:62
#, kde-format
msgid "No rules for specific windows are currently set"
msgstr "Žádná pravidla pro konkrétní okna nejsou momentálně nastavena"

#: ui/main.qml:63
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add New…</interface> button below to add some"
msgstr "Klikněte na tlačítko <interface>Přidat nové...</interface> pro přidání"

#: ui/main.qml:71
#, kde-format
msgid "Select the rules to export"
msgstr ""

#: ui/main.qml:75
#, kde-format
msgid "Unselect All"
msgstr "Zrušit výběr"

#: ui/main.qml:75
#, kde-format
msgid "Select All"
msgstr "Vybrat vše"

#: ui/main.qml:89
#, kde-format
msgid "Save Rules"
msgstr "Uložit pravidla"

#: ui/main.qml:100
#, kde-format
msgid "Add New…"
msgstr "Přidat nové…"

#: ui/main.qml:111
#, kde-format
msgid "Import…"
msgstr "Importovat…"

#: ui/main.qml:119
#, kde-format
msgid "Cancel Export"
msgstr "Zrušit export"

#: ui/main.qml:119
#, kde-format
msgid "Export…"
msgstr "Exportovat…"

#: ui/main.qml:209
#, kde-format
msgid "Edit"
msgstr "Upravit"

#: ui/main.qml:218
#, kde-format
msgid "Duplicate"
msgstr "Duplikovat"

#: ui/main.qml:227
#, kde-format
msgid "Delete"
msgstr "Smazat"

#: ui/main.qml:240
#, kde-format
msgid "Import Rules"
msgstr "Importovat pravidla"

#: ui/main.qml:252
#, kde-format
msgid "Export Rules"
msgstr "Exportovat pravidla"

#: ui/OptionsComboBox.qml:35
#, kde-format
msgid "None selected"
msgstr "Nic nevybráno"

#: ui/OptionsComboBox.qml:41
#, kde-format
msgid "All selected"
msgstr "Vše označené"

#: ui/OptionsComboBox.qml:43
#, kde-format
msgid "%1 selected"
msgid_plural "%1 selected"
msgstr[0] "%1 vybráno"
msgstr[1] "%1 vybrány"
msgstr[2] "%1 vybráno"

#: ui/RulesEditor.qml:63
#, kde-format
msgid "No window properties changed"
msgstr "Nebyly změněny žádné vlastnosti okna"

#: ui/RulesEditor.qml:64
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add Property...</interface> button below to add some "
"window properties that will be affected by the rule"
msgstr ""
"Klikněte na tlačítko <interface>Přidat vlastnost...</interface> níže a "
"přidejte některé vlastnosti okna, které budou pravidlem ovlivněny"

#: ui/RulesEditor.qml:85
#, kde-format
msgid "Close"
msgstr "Zavřít"

#: ui/RulesEditor.qml:85
#, kde-format
msgid "Add Property..."
msgstr "Přidat vlastnost..."

#: ui/RulesEditor.qml:98
#, kde-format
msgid "Detect Window Properties"
msgstr "Detekovat vlastnosti okna"

#: ui/RulesEditor.qml:114 ui/RulesEditor.qml:121
#, kde-format
msgid "Instantly"
msgstr "Okamžitě"

#: ui/RulesEditor.qml:115 ui/RulesEditor.qml:126
#, kde-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] "Po %1 sekundě"
msgstr[1] "Po %1 sekundách"
msgstr[2] "Po %1 sekundách"

#: ui/RulesEditor.qml:175
#, kde-format
msgid "Add property to the rule"
msgstr "Přidat do pravidla vlastnost"

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:54
#, kde-format
msgid "Yes"
msgstr "Ano"

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:60
#, kde-format
msgid "No"
msgstr "Ne"

#: ui/RulesEditor.qml:278 ui/ValueEditor.qml:168 ui/ValueEditor.qml:175
#, kde-format
msgid "%1 %"
msgstr "%1 %"

#: ui/RulesEditor.qml:280
#, kde-format
msgctxt "Coordinates (x, y)"
msgid "(%1, %2)"
msgstr "(%1, %2)"

#: ui/RulesEditor.qml:282
#, kde-format
msgctxt "Size (width, height)"
msgid "(%1, %2)"
msgstr "(%1, %2)"

#: ui/ValueEditor.qml:203
#, kde-format
msgctxt "(x, y) coordinates separator in size/position"
msgid "x"
msgstr "x"
